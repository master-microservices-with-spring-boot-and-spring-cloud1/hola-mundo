package com.in28minutes.rest.webservices.restfulwebservices;

public class HelloWorldBean {

	private String message;
	
	//Definimos un HelloWorldBean que acepta un mensaje
	public HelloWorldBean(String message) {
		this.message = message;
	}
	
	
	//Creamos el Getters 
	public String getMessage() {
		return message;
	}

	//Creamos el Setter 
	public void setMessage(String message) {
		this.message = message;
	}

	//Devolvemos en el método controlador
	@Override
	public String toString() {
		return "HelloWorldBean [message=" + message + "]";
	}

}
