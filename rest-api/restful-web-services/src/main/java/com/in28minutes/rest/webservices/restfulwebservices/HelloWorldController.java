/* Primer ejercicio "Hello World" usando microservicios 
 * Omar Frausto Hernández */ 

package com.in28minutes.rest.webservices.restfulwebservices;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

/*Controller
  Le decimos a Spring MVC que queremos manejar las solicitudes de REST*/
@RestController
public class HelloWorldController {

	//Asignamos una solicitud GET a nuestro URI
	@GetMapping(path = "/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
	//Hello world en archivo JSON, creando la clase Hello World Bean
		@GetMapping(path = "/hello-world-bean")
		public HelloWorldBean helloWorldBean() {
			return new HelloWorldBean("Hello World");
		}
		
		//hello-world/path-variable/in28minutes Usando un path
		@GetMapping(path = "/hello-world/path-variable/{name}")
		public HelloWorldBean helloWorldPathVariable(@PathVariable String name) {
			return new HelloWorldBean(String.format("Hello World, %s", name));
		}
}
